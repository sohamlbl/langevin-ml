import torch
import numpy as np
import torch.nn as nn
from random import randrange
import torch.optim as optim

N = 1000
grid_size = N
hidden_dim = 8
embedding_dim = 8
lstm_unit = 64
batch_size = 1
timesteps = 100
n_layers = 1
output_size=1
epoch_range = 300

! put langevin outputs here
inputs = torch.LongTensor([[randrange(0,grid_size) for i in range(timesteps)]])

hidden_state = torch.randn(n_layers, batch_size, hidden_dim)
cell_state = torch.randn(n_layers, batch_size, hidden_dim)
outputs = torch.randn(batch_size*timesteps, output_size)

# torch.cuda.is_available() checks and returns a Boolean True if a GPU is available, else it'll return False
is_cuda = torch.cuda.is_available()

# If we have a GPU available, we'll set our device to GPU. We'll use this device variable later in our code.
if is_cuda:
    print ("GPU available")
    device = torch.device("cuda")
else:
    print ("GPU not available")
    device = torch.device("cpu")
inputs.to(device)
hidden_state.to(device)
cell_state.to(device)
hidden = (hidden_state, cell_state)
outputs.to(device)

class lanvNet(nn.Module):
    def __init__(self, vocab_size, output_size, embedding_dim, hidden_dim, n_layers):
        super(lanvNet, self).__init__()
        self.output_size = output_size
        self.n_layers = n_layers
        self.hidden_dim = hidden_dim
        
        self.embedding = nn.Embedding(vocab_size, embedding_dim)
        # Output dimensionality hidden_dim.
        self.lstm = nn.LSTM(embedding_dim, hidden_dim, n_layers, batch_first=True)
        self.fc = nn.Linear(hidden_dim, output_size)
        self.softmax = nn.Softmax(dim=1)
        
    def forward(self, x, hidden):
        # Input shape to embedding is (batch_size, seq_length)
        # The input tensor spans a space of vocab_size (x-grid_size) 
        embeds = self.embedding(x)
        lstm_out, hidden = self.lstm(embeds, hidden)
        #change shape from (batch_size, seq_length, hidden_dim) to (batch_size*seq_length, hidden_dim)
        linear_in = lstm_out.reshape(-1, hidden_dim)
        # The fully connected linear layer that maps from LSTM hidden state space to LSTM output space
        linear_out = self.fc(lstm_out)
        # shape of linear_out is (batch_size*seq_length, output_size)
        out = self.softmax(linear_out)
        
        return out, hidden
    
model = lanvNet(grid_size, output_size, embedding_dim, hidden_dim, n_layers)
params =  list(model.parameters())
#print (len(params))
#model.to(device)
optimizer = optim.SGD(model.parameters(), lr=0.1)
for epoch in range(100):
    # Step 1. Remember that Pytorch accumulates gradients.
    # We need to clear them out before each instance
    model.zero_grad()
    # run the model
    outputs, hidden_after = model(inputs, hidden)
    #print (outputs.size())
    #print (outputs)
    #calculate entropy loss
    loss = torch.sum(outputs*torch.log(outputs), dim=(0,1))
    loss.backward()
    optimizer.step()
